package fundamentals;

import java.util.Stack;

public class CustomQueue{
    private Stack<Integer> first = new Stack<>();
    private Stack<Integer> second = new Stack<>();
    private int head;
    public CustomQueue(){

    }

    public void enqueue(int item){
        if(first.isEmpty()){
            head = item;
        }
        first.push(item);
    }

    public int getHead(){
        if(!second.isEmpty()){
            return second.peek();
        }
        return head;
    }

    public int dequeue() throws Exception{
        if(first.isEmpty() && second.isEmpty()){
            throw new Exception("Queue is empty, cannot deque.");
        }
        if(second.empty()){
           while(!first.isEmpty()){
               second.push(first.pop());
           }
        }

        return second.pop();
    }
}