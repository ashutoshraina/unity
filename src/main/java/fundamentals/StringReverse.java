package fundamentals;

public class StringReverse {
    
    public static String reverseWord(String data){
        if(data == null || data.isEmpty()){
            return data;
        }
        char[] temp = data.toCharArray();
        int start = 0;
        int end = data.length() - 1; 
        for( ; start < end; start++, end--){
            char ch = temp[start];
            temp[start] = temp[end];
            temp[end] = ch;
        }

        return new String(temp);
    }

    public static String reverseSentence(String sentence){
        if(sentence == null || sentence.isEmpty()){
            return sentence;
        }

        String[] tokenised = sentence.split(" ");
        int length = tokenised.length;
        int start = 0;
        int end = length - 1;

        for(; start < end; start++, end--){
            String temp = reverseWord(tokenised[start]);
            tokenised[start] = reverseWord(tokenised[end]);
            tokenised[end] = temp;
        }

        if(length % 2 != 0){
            tokenised[length/2] = reverseWord(tokenised[length/2]);
        }
        return String.join(" ", tokenised);
    }
}
