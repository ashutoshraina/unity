package backend;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FileOut implements Output {
  public void write(String msg) throws RuntimeException {
    try {
      BufferedWriter writer = new BufferedWriter(new FileWriter("/tmp/log", true));
      writer.append(msg);
      writer.close();
    } catch (IOException e) {
      throw new RuntimeException("Could not open the log file for writing. Reason : " + e.getMessage());

    }
  }

}
