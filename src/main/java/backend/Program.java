package backend;

public class Program {
    public static void main(String[] args) {
        System.out.println("Starting the logger program.");

        Logger debugLogger = new StandardLogger(Logger.LOG_LEVEL_DEBUG, new StdOut());
        debugLogger.Debug("Logger prints Debug statement \n");
        
        Logger errLogger = new StandardLogger(Logger.LOG_LEVEL_ERROR, new StdErr());
        errLogger.Error("We are logging to stderr. \n");
        
        Logger fileLogger = new StandardLogger(Logger.LOG_LEVEL_DEBUG, new FileOut());
        fileLogger.Debug("We are writing to a file.");

    }
}