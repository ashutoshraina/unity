package fundamentals;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestReverseString {
    
    @Test
    public void shouldReverseAWord(){
        String data = "hello";
        String result = StringReverse.reverseWord(data);
        assertEquals("olleh", result);

        data = "hell";
        result = StringReverse.reverseWord(data);
        assertEquals("lleh", result);
    }

    @Test
    public void shouldReturnAnEmptyStringAsIs(){
        String data = "";
        String result = StringReverse.reverseWord(data);
        assertEquals("", result);

        data = null;
        result = StringReverse.reverseWord(data);
        assertEquals(null, result);
    }

    @Test
    public void shouldReverseASentence(){
        String data = "foobar baz";
        String result = StringReverse.reverseSentence(data);
        assertEquals("zab raboof", result);

        data = "hello how are you ?";
        result = StringReverse.reverseSentence(data);
        assertEquals("? uoy era woh olleh", result);
    }

    @Test
    public void shouldReturnAnEmptyOrNullSentenceAsIs(){
        String data = "";
        String result = StringReverse.reverseSentence(data);
        assertEquals("", result);

        data = null;
        result = StringReverse.reverseSentence(data);
        assertEquals(null, result);
    }
}
