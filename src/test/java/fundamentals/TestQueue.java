package fundamentals;

import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

import org.junit.Rule;

public class TestQueue {
    
    @Rule
    public final ExpectedException exception = ExpectedException.none();
  
    @Test
    public void enqueueShouldAnItemToTheTail(){
        CustomQueue queue = new CustomQueue();
        
        queue.enqueue(10);
        assertEquals(10, queue.getHead());

        queue.enqueue(20);
        assertEquals(10, queue.getHead());
    }

    @Test
    public void dequeueShouldRemoveTheHead() throws Exception{
        CustomQueue queue = new CustomQueue();
       
        queue.enqueue(10);
        queue.enqueue(20);
        int head = queue.dequeue();
        assertEquals(10, head);
        assertEquals(20, queue.getHead());

        queue.enqueue(30);        
        head = queue.dequeue();
        assertEquals(20, head);
        assertEquals(30, queue.getHead());
    }

    @Test
    public void dequeueShouldThrowAnExceptionOnForEmptyQueue() throws Exception{
        CustomQueue queue = new CustomQueue();
        exception.expect(Exception.class);
        queue.dequeue();
        exception.expectMessage("Queue is empty, cannot deque.");
    }
}
