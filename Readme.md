SetUp :

You need to have gradle and java8 installed on your machine.

### Complexity Analysis of Queue :

The queue is implemented using two stacks.

Enqueue has O(1) time complexity and O(n) of space complexity to store the items.
Getting the head of the queue is O(1).
Dequeue has time complexity of O(n) in the worst case when the second stack is empty and we have to pop all the elements from the second stack and add them to the first stack resulting in a total of 2n operations.
When the second stack is not empty then we have a time complexity of O(1). The overall space complexity of O(1).

To run all the tests use ``` gradle test ```

### Section 3 Backend

* The program will print v2 since the put operation of hashtable replaces the value if an existing key is present.

** Logger ..You can run the program using ``` gradle run ```. The main class is set to Program.java.

```
$ gradle run
:compileJava
:processResources UP-TO-DATE
:classes
:run

Starting the logger program.
Logger prints Debug statement
We are logging to stderr.

BUILD SUCCESSFUL

Total time: 5.498 secs
```

You can test the log file using the command below 

```
$ cat /tmp/log
We are writing to a file.%
```